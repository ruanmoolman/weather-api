# City Weather API

## Setup

1. Install _Poetry_ and run `poetry install`
2. Run `poetry shell` to activate the environment

---

**TIP**: The use of _pyenv_ is recommened to manage your python versions.

---

3. Initialize the DB by running the following:

```bash
python manage.py migrate
```

## Setup environment

1. Go to [weather api](https://www.weatherapi.com/) and register an account if you don't have one
2. Copy the API key that is shown on your [account page](https://www.weatherapi.com/my/)
3. Create a `.env` file containing your API key

```bash
echo WEATHER_API_KEY=<API_KEY> > .env
```

## Running a local service

```bash
python manage.py runserver
```

## Testing

Test are run with the following command:

```bash
python manage.py test
```

Some tests call the external weather API. To skip these tests run:

```bash
python manage.py test --exclude-tag=integration
```
