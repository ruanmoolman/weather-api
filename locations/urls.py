from django.urls import include, path

from locations.views import LocationWeatherView

urlpatterns = [path("locations/<str:location>/", LocationWeatherView.as_view())]
