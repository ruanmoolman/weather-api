from statistics import mean, median
from unittest.mock import MagicMock, Mock, patch

from django.test import TestCase, tag

from locations.weather_service import (
    _get_weather_api_forecast,
    get_temperature_forecast,
)


class TestExternalWeatherApi(TestCase):
    @patch("locations.weather_service.requests.get")
    def test_correct_url_is_called(self, mock_request):
        api_key, city, days = "ABCD", "Cape Town", 3
        with self.settings(WEATHER_API_KEY=api_key):
            _get_weather_api_forecast(city, days)
        mock_request.return_value = Mock(ok=True)
        mock_request.assert_called_once_with(
            "http://api.weatherapi.com/v1/forecast.json",
            params={
                "key": api_key,
                "q": city,
                "days": days,
                "aqi": "no",
                "alerts": "no",
            },
        )

    @tag("integration", "new")
    def test_response_if_api_key_is_invalid(self):
        city, days = "London", 2
        with self.settings(WEATHER_API_KEY="not-a-valid-key"):
            with self.assertRaises(Exception) as context:
                _get_weather_api_forecast(city, days)
            self.assertEqual(str(context.exception), "API key is invalid.")

    @tag("integration")
    def test_response_if_location_is_not_valid(self):
        city, days = "asdf", 2
        with self.assertRaises(Exception) as context:
            _get_weather_api_forecast(city, days)
        self.assertEqual(str(context.exception), "No matching location found.")

    @tag("integration")
    def test_structure_of_response_data(self):
        city, days = "London", 2
        data = _get_weather_api_forecast(city, days)

        self.assertTrue("forecast" in data)
        self.assertTrue("forecastday" in data["forecast"])

        forecast_days = data["forecast"]["forecastday"]
        self.assertEqual(len(forecast_days), days)

        self.assertTrue("day" in forecast_days[0])
        day_info = forecast_days[0]["day"]
        self.assertTrue("maxtemp_c" in day_info)
        self.assertTrue("mintemp_c" in day_info)
        self.assertTrue("avgtemp_c" in day_info)

        self.assertTrue("hour" in forecast_days[0])
        hour_info = forecast_days[0]["hour"]
        self.assertEqual(len(hour_info), 24)
        self.assertTrue("temp_c" in hour_info[0])


def create_mock_data(location, days_temperatures):
    return {
        "location": {"name": location},
        "forecast": {
            "forecastday": [
                {
                    "day": {
                        "maxtemp_c": round(max(day_temperatures), 1),
                        "mintemp_c": round(min(day_temperatures), 1),
                        "avgtemp_c": round(
                            sum(day_temperatures) / len(day_temperatures), 1
                        ),
                    },
                    "hour": [
                        {"temp_c": hour_temperature}
                        for hour_temperature in day_temperatures
                    ],
                }
                for day_temperatures in days_temperatures
            ]
        },
    }


@patch("locations.weather_service._get_weather_api_forecast")
class TestGetTemperateForecast(TestCase):
    def test_api_exceptions_passed_on(self, mock_fn):
        mock_fn.side_effect = Exception("API error.")
        with self.assertRaises(Exception) as context:
            get_temperature_forecast("London", 2)
        self.assertEqual(str(context.exception), "API error.")

    def test_result_with_same_temperate_over_multiple_days(self, mock_fn):
        day_1_temps = [12.0] * 24
        day_2_temps = [12.0] * 24

        mock_fn.return_value = create_mock_data("London", [day_1_temps, day_2_temps])

        forecast = get_temperature_forecast("London", 2)
        self.assertEqual(
            forecast,
            {
                "average": 12.0,
                "maximum": 12.0,
                "minimum": 12.0,
                "median": 12.0,
            },
        )

    def test_result_with_different_temperate_values(self, mock_fn):
        day_1_temps = [15.8, 12.2, 13.9]
        day_2_temps = [11.3, 11.5, 11.7]
        day_3_temps = [13.5, 10.9, 11.2]
        all_temps = day_1_temps + day_2_temps + day_3_temps

        mock_fn.return_value = create_mock_data(
            "Cape Town", [day_1_temps, day_2_temps, day_3_temps]
        )

        self.assertEqual(
            get_temperature_forecast("Cape Town", 3),
            {
                "average": round(mean(all_temps), 1),
                "maximum": round(max(all_temps), 1),
                "minimum": round(min(all_temps), 1),
                "median": round(median(all_temps), 1),
            },
        )
