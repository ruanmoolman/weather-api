from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from locations.weather_service import get_temperature_forecast


class LocationWeatherView(APIView):
    """View that returns weather predictions for a specific location"""

    def get(self, request, location):
        """Get a summary of the temperate forecast for a location.

        Args:
            request (rest_framework.Request): The request object associated with the call
            location (str): A string representing the location, eg. "london" or "capetown"

        Returns:
            rest_framework.Response: The response returned to the calling client
        """
        days = request.query_params.get("days")
        if days is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data="'days' query parameter not specified",
            )
        try:
            days = int(days)
        except ValueError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data="'days' query parameter must be an integer",
            )
        try:
            return Response(
                get_temperature_forecast(location, days),
                content_type="application/json",
            )
        except Exception as ex:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=str(ex))
