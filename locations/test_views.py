from unittest.mock import patch

from django.test import TestCase
from rest_framework import status


@patch("locations.views.get_temperature_forecast")
class TestLocationsView(TestCase):
    def test_returns_data_from_get_temperature_forecast(self, mock_fn):
        returned_data = {
            "average": 10.0,
            "maximum": 20.0,
            "minimum": -12.0,
            "median": 8.0,
        }
        mock_fn.return_value = returned_data

        response = self.client.get("/api/locations/London/", data={"days": 3})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), returned_data)

    def test_returns_500_if_get_temperature_forecast_returns_none(self, mock_fn):
        mock_fn.side_effect = Exception("API error.")

        response = self.client.get("/api/locations/Berlin/", data={"days": 2})

        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.assertEqual(response.json(), "API error.")

    def test_returns_400_if_days_query_parameter_is_not_specified(self, mock_fn):
        mock_fn.return_value = None

        response = self.client.get("/api/locations/Cairo/")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), "'days' query parameter not specified")

    def test_returns_400_if_days_query_parameter_is_not_an_integer(self, mock_fn):
        mock_fn.return_value = None

        response = self.client.get("/api/locations/Durban/", data={"days": "asd"})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), "'days' query parameter must be an integer")
