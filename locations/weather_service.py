from statistics import mean, median
from typing import Tuple

import requests
from django.conf import settings

WEATHER_API_BASE_URL = "http://api.weatherapi.com/v1"


def _get_weather_api_forecast(city: str, days: int) -> Tuple:
    params = {
        "key": settings.WEATHER_API_KEY,
        "q": city,
        "days": days,
        "aqi": "no",
        "alerts": "no",
    }
    response = requests.get(f"{WEATHER_API_BASE_URL}/forecast.json", params=params)

    if not response.ok:
        raise Exception(response.json()["error"]["message"])

    return response.json()


def get_temperature_forecast(city: int, days: int):
    """Get a summary of the temperate forecast for the specified `city` over
    the next number of `days`.

    Args:
        city (str): The city to get the temperate information for
        days (int): The number of days' information to use

    Returns:
        dict: The predicted temperature data
    """
    forecast_data = _get_weather_api_forecast(city, days)
    forecast_days = forecast_data["forecast"]["forecastday"]

    days_maximums = [forecast_day["day"]["maxtemp_c"] for forecast_day in forecast_days]
    days_minimums = [forecast_day["day"]["mintemp_c"] for forecast_day in forecast_days]

    hourly_temperatures = []
    for forecast_day in forecast_days:
        hourly_temperatures += [
            hour_info["temp_c"] for hour_info in forecast_day["hour"]
        ]

    return {
        "maximum": round(max(days_maximums), 1),
        "minimum": round(min(days_minimums), 1),
        "average": round(mean(hourly_temperatures), 1),
        "median": round(median(hourly_temperatures), 1),
    }
